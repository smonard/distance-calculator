# frozen_string_literal: true

require 'sinatra'
require 'sinatra/json'

data_base = {
  userA: { following: %w[userB userD userE userG], followers: %w[] },
  userB: { following: %w[userC userJ userI userE], followers: %w[userA] },
  userC: { following: %w[userM userN userJ userI userE], followers: %w[userB] },
  userZ: { following: %w[userS], followers: %w[userS] }
}

get '/:username/following' do
  following = data_base[params[:username].to_sym]&.dig(:following) || []
  response = { user: params[:username], Following: following }
  [200, {}, response.to_json]
end

get '/:username/followers' do
  followers = data_base[params[:username].to_sym]&.dig(:followers) || []
  response = { user: params[:username], Followers: followers }
  [200, {}, response.to_json]
end
