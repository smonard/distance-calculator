#!/usr/bin/env ruby
# frozen_string_literal: true

$LOAD_PATH.unshift File.join(__dir__, 'src')

require 'pry'
require 'social_network'

begin
  args = ARGV.reject(&:empty?)
  raise ArgumentError, 'Two arguments are expected.' if args.size != 2

  final = SocialNetwork.calculate_distance_between(args[0], args[1])
  puts(final.nil? ? 'Non-related users' : "Distance #{final}")
rescue StandardError => e
  puts "Error. #{e.message}"
  exit(1)
end
