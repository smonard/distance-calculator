# frozen_string_literal: true

class PriorityQueue
  def initialize
    @elements = []
  end

  def <<(element)
    @elements << element
  end

  def +(other)
    @elements += other
    @elements.uniq!
  end

  def empty?
    @elements.empty?
  end

  def pop
    @elements.delete @elements.first
  end
end
