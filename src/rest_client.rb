# frozen_string_literal: true

require 'net/http'
require 'json'
require 'yaml'

class RestClient
  @@remote = lambda {
    config = YAML.load_file('config.yml')['api']
    "#{config['scheme']}://#{config['host']}:#{config['port']}"
  }.call

  def get_followers(username)
    url = "#{@@remote}/#{username}/followers"
    get_from_host(url)[:followers]
  end

  def get_following(username)
    url = "#{@@remote}/#{username}/following"
    get_from_host(url)[:following]
  end

  private

  def get_from_host(url)
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    http.open_timeout = 90
    http.read_timeout = 120
    JSON.parse(http.get(uri.request_uri).body, symbolize_names: true).transform_keys { |k| k.downcase.to_sym }
  end
end
