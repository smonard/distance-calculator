# frozen_string_literal: true

require 'user'
require 'priority_queue'

class SocialNetwork
  @@open_set = PriorityQueue.new
  @@closed_set = []

  def self.calculate_distance_between(init_username, goal_username)
    goal_user = User.find(goal_username, nil)
    init_user = User.find(init_username, nil)
    init_user.calculate_distance_to goal_user
  end

  def self.closed_set
    @@closed_set
  end

  def self.open_set
    @@open_set
  end
end
