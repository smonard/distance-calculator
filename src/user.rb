# frozen_string_literal: true

require 'rest_client'

class User
  include Comparable
  attr_reader :username, :following, :followers, :parent
  @@webclient = RestClient.new

  def initialize(username, following, followers, parent)
    @username = username
    @following = following
    @followers = followers
    @parent = parent
  end

  def calculate_distance_to(goal_user)
    SocialNetwork.closed_set << username
    loop do
      return trace_jumps(self) if self == goal_user

      SocialNetwork.open_set + related_users.filter { |chl| SocialNetwork.closed_set.none? chl[:child] }
      break if SocialNetwork.open_set.empty?

      next_key = SocialNetwork.open_set.pop
      next_user = User.find next_key[:child], next_key[:root]
      return next_user.calculate_distance_to goal_user
    end
    nil
  end

  def <=>(other)
    @username <=> other.username
  end

  def self.find(username, parent)
    followers = @@webclient.get_followers username
    following = @@webclient.get_following username
    User.new username, following, followers, parent
  end

  private

  def trace_jumps(usr, jumps = 0)
    usr.parent.nil? ? jumps : trace_jumps(usr.parent, jumps + 1)
  end

  def related_users
    (@following + @followers).map { |chl| { root: self, child: chl } }
  end
end
