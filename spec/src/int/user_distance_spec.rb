# frozen_string_literal: true

RSpec.describe 'UserDistance' do
  context 'when related users' do
    it 'returns 0 for the same user' do
      main_file = './main.rb userA userA'
      output = `#{main_file}`
      expect(output).to include 'Distance 0'
    end

    it 'returns 1 for direct follower' do
      main_file = './main.rb userA userB'
      output = `#{main_file}`
      expect(output).to include 'Distance 1'
    end

    it 'returns 1 for user that follows target' do
      main_file = './main.rb userB userA'
      output = `#{main_file}`
      expect(output).to include 'Distance 1'
    end

    it 'returns 3 for users with 3 jumps of distance' do
      main_file = './main.rb userA userM'
      output = `#{main_file}`
      expect(output).to include 'Distance 3'
    end
  end

  context 'when related users' do
    it 'returns a message saying that users are not related when no conection exists' do
      main_file = './main.rb userA userZ'
      output = `#{main_file}`
      expect(output).to include 'Non-related users'
    end
  end
end
