# Distance Calculator

Code for calculate distances between users within a social network. It offers a command-line interface.
The selected approach to solve the problem was to apply an offline search, concretely a breadth-first search (search trees). The code was built keeping in mind scalation to a heuristic search.

# Setup

The program is based in the Ruby programming language so you must install the Ruby interpreter in order to run it.

This program was built using the following ruby version: `ruby 2.6.3p62 (2019-04-16 revision 67580) [x86_64-linux]`

Once the ruby interpreter is installed, all the project dependencies must be installed by typing: 

`bundle install` (you may need to install the Bundler `gem install bundler`)


# Run the program

You can run the program by typing:

`bundle exec ruby main.rb 'userA' 'userB'` or `ruby main.rb 'userA' 'userB'` or `./main.rb 'userA' 'userB'`

It receives two arguments, the two users that the distance between them is required.

Note: A valid API must be running in order to succeed. You can run the mock API provided in this repository: `bundle exec ruby mock_server.rb` or `ruby mock_server.rb`. This mock server must be initiated in another process/terminal-tab


# Run tests

Rspec is required to run tests. To run tests, it's only necessary:
`bundle exec rspec`
Please note that the mock server must be running (`bundle exec ruby mock_server.rb` or `ruby mock_server.rb`)